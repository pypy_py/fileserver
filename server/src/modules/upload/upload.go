package upload;

import (
	"github.com/labstack/echo/v4"
	"path/filepath"
	"goref/src/modules/file"
	"goref/src/config"
	"net/http"
	"fmt"
);

type Response struct {
	Error string `json:"error" xml:"error"`
	Data interface{} `json:"data" xml:"data"`
}

type File struct {
	Name string `json:"name" xml:"name"`
}

func GetFiles(ctx echo.Context) error {
	files, err := file.ReadDir(file.DataDirectory);
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, &Response {});
	}

	var filenames []string;

	for _, item := range files {
		filenames = append(filenames, config.BASE_URL + "/" + file.DataURL + "/" + item.Name());
	}

	return ctx.JSON(http.StatusOK, &Response { Data: filenames });
}

func DeleteFile(ctx echo.Context) error {
	data := new(File);

	if err := ctx.Bind(data); err != nil {
		fmt.Println(err);
		return err;
	}

	fmt.Println("Deleting file: " + data.Name);

	if err := file.Delete(file.DataDirectory + "/" + data.Name); err != nil {
		return ctx.JSON(http.StatusInternalServerError, &Response {});
	}

	return ctx.JSON(http.StatusOK, &Response {});
}

func UploadFile(ctx echo.Context) error {
	fmt.Println("Received upload request");

	name := ctx.FormValue("name");
	if name == "" {
		return ctx.JSON(http.StatusBadRequest, &Response {});
	}

	fileData, err := ctx.FormFile("file")
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, &Response {});
	}

	filename := name + filepath.Ext(fileData.Filename);
	err = file.Create(file.DataDirectory + "/" + filename, *fileData);
	if err != nil {
		fmt.Println(err);
		return ctx.JSON(http.StatusInternalServerError, &Response {});
	}

	return ctx.JSON(http.StatusOK, &Response {});
}
