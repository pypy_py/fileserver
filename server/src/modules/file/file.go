package file

import (
	"mime/multipart"
	"os"
	"io"
);

const DataDirectory = "data";
const DataURL = "data";

func InitDataDirectory() {
	_, err := os.Stat(DataDirectory);
	if os.IsNotExist(err) {
		os.Mkdir(DataDirectory, 0755);
	}
}

func Read(path string) {
	return;
}

func ReadDir(path string) ([]os.DirEntry, error) {
	files, err := os.ReadDir(path);
	if err != nil {
		return nil, err;
	}
	return files, nil;
}

func Delete(path string) error {
	return os.Remove(path);
}

func Create(path string, file multipart.FileHeader) error {
	src, err := file.Open();
	if err != nil {
		return err;
	}

	defer src.Close();

	dst, err := os.Create(path);
	if err != nil {
		return err;
	}
	defer dst.Close();

	if _, err = io.Copy(dst, src); err != nil {
		return err;
	}

	return nil;
}
