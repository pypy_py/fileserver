# File Server
A barebones web file manager made with Go and Vue + TypeScript. Somewhat tediously coded in Emacs (LSP-mode can be a pain sometimes), but it was fun nevertheless.
